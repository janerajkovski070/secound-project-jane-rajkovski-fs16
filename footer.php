<footer class="footer mt-auto py-3 bg-gradient">
    <div class="container text-center">
        <div id="quote-container">
            <p>Loading quote...</p>
        </div>
    </div>
</footer>

<script>
    // Fetch random quote from API
    fetch('http://api.quotable.io/random')
        .then(response => response.json())
        .then(data => {
            document.getElementById('quote-container').innerHTML = `<blockquote class="blockquote">${data.content}<br><hr><footer class="blockquote-footer">${data.author}</footer></blockquote>`;
        })
        .catch(error => console.error('Error fetching quote:', error));
</script>
