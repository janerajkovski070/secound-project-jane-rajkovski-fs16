<?php
include 'config.php';

if (!function_exists('isLoggedIn')) {
    function isLoggedIn() {
        return isset($_SESSION['user_id']);
    }
}

if (!function_exists('checkUserExists')) {
    function checkUserExists($username, $email, $conn) {
        $stmt = $conn->prepare("SELECT id FROM users WHERE username = ? OR email = ?");
        $stmt->bind_param("ss", $username, $email);
        $stmt->execute();
        $stmt->store_result();

        $exists = $stmt->num_rows > 0;
        $stmt->close();

        return $exists;
    }
}

if (!function_exists('validateInputs')) {
    function validateInputs($username, $email, $password) {
        $errors = [];

        if (empty($username)) {
            $errors[] = 'Username is required';
        } elseif (strlen($username) < 4) {
            $errors[] = 'Username must be at least 4 characters long.';
        }

        if (empty($email) || !filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $errors[] = 'A valid email is required';
        }

        if (empty($password)) {
            $errors[] = 'Password is required';
        } elseif (strlen($password) < 8) {
            $errors[] = 'Password must be at least 8 characters long.';
        }

        if (!preg_match('/\d/', $password)) {
            $errors[] = 'Password must contain at least one number.';
        }

        return $errors;
    }
}

if (!function_exists('insertNoteInDatabase')) {
    function insertNoteInDatabase($user_id, $book_id, $note) {
        global $pdo;

        try {
            $stmt = $pdo->prepare("INSERT INTO private_notes (user_id, book_id, note, created_at) VALUES (:user_id, :book_id, :note, NOW())");
            $stmt->bindParam(':user_id', $user_id, PDO::PARAM_INT);
            $stmt->bindParam(':book_id', $book_id, PDO::PARAM_INT);
            $stmt->bindParam(':note', $note, PDO::PARAM_STR);
            $stmt->execute();

            return $pdo->lastInsertId();
        } catch (PDOException $e) {
            die("Failed to insert private note: " . $e->getMessage());
        }
    }
}

if (!function_exists('getPrivateNotesForBook')) {
    function getPrivateNotesForBook($user_id, $book_id) {
        global $pdo;

        try {
            $stmt = $pdo->prepare("SELECT * FROM private_notes WHERE user_id = :user_id AND book_id = :book_id AND deleted_at IS NULL ORDER BY created_at DESC");
            $stmt->bindParam(':user_id', $user_id, PDO::PARAM_INT);
            $stmt->bindParam(':book_id', $book_id, PDO::PARAM_INT);
            $stmt->execute();
            $notes = $stmt->fetchAll(PDO::FETCH_ASSOC);

            return $notes;
        } catch (PDOException $e) {
            die("Failed to fetch private notes: " . $e->getMessage());
        }
    }
}

if (!function_exists('softDeletePrivateNote')) {
    function softDeletePrivateNote($note_id) {
        global $pdo;

        try {
            $stmt = $pdo->prepare("UPDATE private_notes SET deleted_at = NOW() WHERE id = :note_id");
            $stmt->bindParam(':note_id', $note_id, PDO::PARAM_INT);
            $stmt->execute();

            return true;
        } catch (PDOException $e) {
            die("Failed to delete private note: " . $e->getMessage());
        }
    }
}

function getUserCommentsCount($conn, $user_id, $book_id) {
    $stmt = $conn->prepare("SELECT COUNT(*) FROM comments WHERE user_id = ? AND book_id = ?");
    $stmt->bind_param("ii", $user_id, $book_id);
    $stmt->execute();
    $stmt->bind_result($count);
    $stmt->fetch();
    $stmt->close();
    return $count;
}
