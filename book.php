<?php
session_start();
include_once 'config.php';

$book_id = isset($_GET['book_id']) ? intval($_GET['book_id']) : 0;
$notes = [];
$commentsPending = [];
$commentsApproved = [];
$userCommentsCount = 0;
$book = null;

if ($book_id > 0) {

    $stmt = $conn->prepare("
        SELECT 
            books.*, 
            authors.first_name AS author_first_name, 
            authors.last_name AS author_last_name, 
            categories.title AS category_title 
        FROM 
            books 
        JOIN 
            authors ON books.author_id = authors.id 
        JOIN 
            categories ON books.category_id = categories.id 
        WHERE 
            books.id = ?
    ");
    $stmt->bind_param("i", $book_id);
    $stmt->execute();
    $result = $stmt->get_result();
    $book = $result->fetch_assoc();
    $stmt->close();

    // Fetch comments
    $commentsStmt = $conn->prepare("
        SELECT 
            comments.*, 
            users.username 
        FROM 
            comments 
        JOIN 
            users ON comments.user_id = users.id 
        WHERE 
            comments.book_id = ? 
        ORDER BY 
            comments.created_at DESC
    ");
    $commentsStmt->bind_param("i", $book_id);
    $commentsStmt->execute();
    $commentsResult = $commentsStmt->get_result();
    while ($row = $commentsResult->fetch_assoc()) {
        if ($row['approved'] == 1) {
            $commentsApproved[] = $row;
        } elseif (isLoggedIn() && $row['approved'] == 0 && $row['user_id'] == $_SESSION['user_id']) {
            $commentsPending[] = $row;
        }
        if (isLoggedIn() && $row['user_id'] == $_SESSION['user_id']) {
            $userCommentsCount++;
        }
    }
    $commentsStmt->close();

    // Fetch notes
    if (isLoggedIn()) {
        $user_id = $_SESSION['user_id'];
        $notesStmt = $conn->prepare("
            SELECT id, note, created_at 
            FROM private_notes 
            WHERE user_id = ? 
            AND book_id = ? 
            AND deleted_at IS NULL
            ORDER BY created_at DESC
        ");
        $notesStmt->bind_param("ii", $user_id, $book_id);
        $notesStmt->execute();
        $notesResult = $notesStmt->get_result();
        while ($note = $notesResult->fetch_assoc()) {
            $notes[] = $note;
        }
        $notesStmt->close();
    }
}


// Handle form submission for comments and notes
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if (isLoggedIn()) {
        $user_id = $_SESSION['user_id'];

        // Add Note
        if (isset($_POST['note'])) {
            $note = htmlspecialchars(trim($_POST['note']));
            if ($note && $book_id > 0) {
                $stmt = $conn->prepare("INSERT INTO private_notes (user_id, book_id, note, created_at, deleted_at) VALUES (?, ?, ?, NOW(), NULL)");
                $stmt->bind_param("iis", $user_id, $book_id, $note);
                $stmt->execute();
                $stmt->close();

                // Fetch updated notes
                $notes = [];
                $notesStmt = $conn->prepare("
                    SELECT id, note, created_at 
                    FROM private_notes 
                    WHERE user_id = ? 
                    AND book_id = ? 
                    AND deleted_at IS NULL
                    ORDER BY created_at DESC
                ");
                $notesStmt->bind_param("ii", $user_id, $book_id);
                $notesStmt->execute();
                $notesResult = $notesStmt->get_result();
                while ($note = $notesResult->fetch_assoc()) {
                    $notes[] = $note;
                }
                $notesStmt->close();
            }
        // Edit Note
        } elseif (isset($_POST['edit_note'])) {
            $note_id = $_POST['edit_note_id'];
            $note = htmlspecialchars(trim($_POST['edit_note']));
            $stmt = $conn->prepare("UPDATE private_notes SET note = ? WHERE id = ? AND user_id = ?");
            $stmt->bind_param("sii", $note, $note_id, $user_id);
            $stmt->execute();
            $stmt->close();

            header("Location: book.php?book_id=$book_id");
            exit();
        // Delete Note
        } elseif (isset($_POST['delete_note_id'])) {
            $note_id = $_POST['delete_note_id'];
            $stmt = $conn->prepare("UPDATE private_notes SET deleted_at = NOW() WHERE id = ? AND user_id = ?");
            $stmt->bind_param("ii", $note_id, $user_id);
            $stmt->execute();
            $stmt->close();

            header("Location: book.php?book_id=$book_id");
            exit();
        // Add Comment
        } elseif (isset($_POST['comment'])) {
            if ($userCommentsCount < 2) {
                $comment = htmlspecialchars(trim($_POST['comment']));
                if ($comment && $book_id > 0) {
                    $stmt = $conn->prepare("INSERT INTO comments (user_id, book_id, comment, created_at, approved) VALUES (?, ?, ?, NOW(), 0)");
                    $stmt->bind_param("iis", $user_id, $book_id, $comment);
                    $stmt->execute();
                    $stmt->close();

                    header("Location: book.php?book_id=$book_id");
                    exit();
                }
            }
        // Edit Comment
        } elseif (isset($_POST['edit_comment_id'])) {
            $comment_id = $_POST['edit_comment_id'];
            $comment = htmlspecialchars(trim($_POST['edit_comment']));
            $stmt = $conn->prepare("UPDATE comments SET comment = ? WHERE id = ? AND user_id = ?");
            $stmt->bind_param("sii", $comment, $comment_id, $user_id);
            $stmt->execute();
            $stmt->close();

            header("Location: book.php?book_id=$book_id");
            exit();
        // Delete Comment
        } elseif (isset($_POST['delete_comment_id'])) {
            $comment_id = $_POST['delete_comment_id'];
            $stmt = $conn->prepare("DELETE FROM comments WHERE id = ? AND user_id = ?");
            $stmt->bind_param("ii", $comment_id, $user_id);
            $stmt->execute();
            $stmt->close();

            header("Location: book.php?book_id=$book_id");
            exit();
        }
    }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo htmlspecialchars($book['title'] ?? 'Book Not Found'); ?></title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous" />
    <link rel="stylesheet" href="./styles.css">
</head>
<body>

<?php include 'header.php'; ?>

      <!-- book -->
    <div class="container mt-5">
    <?php if ($book) : ?>
    <div class="row d-flex justify-content-around bd-highlight mb-3">
        <div class="col-md-6 hgh order-md-1 pb-4">
            <img src="<?php echo htmlspecialchars($book['image_url']); ?>" class="img-fluid rounded img-top" alt="Book Cover">
        </div>
        <div class="col-md-6 hgh order-md-2 book-details">
            <h1><?php echo htmlspecialchars($book['title']); ?></h1>
            <p>By: <?php echo htmlspecialchars($book['author_first_name'] . ' ' . $book['author_last_name']); ?></p>
            <p>Category: <?php echo htmlspecialchars($book['category_title']); ?></p>
            <p>Publication Year: <?php echo htmlspecialchars($book['publication_year']); ?></p>
            <p>Page Count: <?php echo htmlspecialchars($book['page_count']); ?></p>
        </div>
    </div>
<?php endif; ?>
      <!-- Add Note Form -->
      <?php if (isLoggedIn()) : ?>
            <div class="notes pb-3" id="notesSection">
                <form action="book.php?book_id=<?php echo $book_id; ?>" method="post">
                    <label for="notes" class="form-label">Add your notes</label>
                    <textarea name="note" id="notes" class="form-control" rows="2"></textarea>
                    <button type="submit" class="btn btn-primary mt-2">Submit</button>
                </form>
            </div>
        <?php endif; ?>
        <!-- Display Notes -->
        <?php if (isLoggedIn()) : ?>
            <h3>Your notes</h3>

            <div id="notesList" class="mb-4 p-1">
                <?php if (!empty($notes)) : ?>
                    <?php foreach ($notes as $note) : ?>
                        <div class="card mb-3" data-note-id="<?php echo $note['id']; ?>">
                            <div class="card-body bg-secondary" data-original-content="<?php echo htmlspecialchars($note['note']); ?>">
                                <p class="card-text"><?php echo htmlspecialchars($note['note']); ?></p>
                                <p class="card-text"><small class="text-muted">Created at <?php echo htmlspecialchars($note['created_at']); ?></small></p>
                                <button class="btn btn-info btn-sm" onclick="editNote(<?php echo $note['id']; ?>, '<?php echo htmlspecialchars(addslashes($note['note'])); ?>')">Edit</button>
                                <form action="book.php?book_id=<?php echo $book_id; ?>" method="post" class="d-inline">
                                    <input type="hidden" name="delete_note_id" value="<?php echo $note['id']; ?>">
                                    <button type="submit" class="btn btn-danger btn-sm">Delete</button>
                                </form>
                            </div>
                        </div>
                    <?php endforeach; ?>
                <?php else : ?>
                    <p>No notes yet.</p>
                <?php endif; ?>
            </div>
        <?php endif; ?>
    <!-- Add Comment Form -->
    <?php if (isLoggedIn() && $userCommentsCount < 2) : ?>
        <h3>Leave a comment</h3>
        <div class="comments pb-3" id="commentsSection">
            <form action="book.php?book_id=<?php echo $book_id; ?>" method="post">
                <label for="comment" class="form-label">Add your comment</label>
                <textarea name="comment" id="comment" class="form-control" rows="2"></textarea>
                <button type="submit" class="btn btn-primary mt-2">Submit</button>
            </form>
        </div>
    <?php endif; ?>
   <!-- Display Comments -->
<?php foreach ($commentsPending as $comment) : ?>
    <h3>Pending comments</h3>
    <div class="row pb-3">
        <div class="col">
            <div class="card bg-secondary m-2">
                <div class="card-body" data-comment-id="<?php echo $comment['id']; ?>">
                    <h5 class="card-title"><?php echo htmlspecialchars($comment['username']); ?></h5>
                    <div class="comment-content">
                        <p class="card-text"><?php echo htmlspecialchars($comment['comment']); ?></p>
                        <p class="card-text"><small class="text-muted">Posted on: <?php echo htmlspecialchars($comment['created_at']); ?></small></p>
                    </div>
                    <!-- Edit Form (initially hidden) -->
                    <form id="editCommentForm-<?php echo $comment['id']; ?>" action="book.php?book_id=<?php echo $book_id; ?>" method="post" style="display: none;">
                        <input type="hidden" name="edit_comment_id" value="<?php echo $comment['id']; ?>">
                        <textarea name="edit_comment" class="form-control" rows="2"><?php echo htmlspecialchars($comment['comment']); ?></textarea>
                        <button type="submit" class="btn btn-primary mt-2">Update</button>
                        <button type="button" class="btn btn-secondary mt-2" onclick="cancelCommentEdit(<?php echo $comment['id']; ?>)">Cancel</button>
                    </form>
                    <!-- Edit Button (initially shown) -->
                    <button class="btn btn-info btn-sm" onclick="showEditCommentForm(<?php echo $comment['id']; ?>)">Edit</button>
                    <!-- Delete Form (initially hidden) -->
                    <form action="book.php?book_id=<?php echo $book_id; ?>" method="post" class="d-inline" style="display: none;">
                        <input type="hidden" name="delete_comment_id" value="<?php echo $comment['id']; ?>">
                        <button type="submit" class="btn btn-danger btn-sm">Delete</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
<?php endforeach; ?>
<!-- add all coments aproved -->
  <div class="mb-4 pb-4">
        <h4>Comments</h4>
        <?php if (!empty($commentsApproved)) : ?>
            <?php foreach ($commentsApproved as $comment) : ?>
                <div class="mb-3 card bg-secondary m-2 p-3">
                    <p><?php echo nl2br(htmlspecialchars($comment['comment'])); ?></p>
                    <small class="text-muted">By <b><?php echo htmlspecialchars($comment['username']); ?></b> on <?php echo htmlspecialchars($comment['created_at']); ?></small>
                </div>
            <?php endforeach; ?>
        <?php else : ?>
            <p>No comments available.</p>
        <?php endif; ?>
    </div>
    <script>
       function addNote() {
    const note = document.getElementById('note').value.trim();
    if (note !== '') {
        fetch('book.php', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
            },
            body: `note=${encodeURIComponent(note)}`,
        })
        .then(response => {
            if (response.ok) {
                return response.json();
            }
            throw new Error('Network response was not ok.');
        })
        .then(data => {
            location.reload();
        })
        .catch(error => {
            console.error('Error adding note:', error);
        });
    } else {
        alert('Please enter a valid note.');
    }
}

function editNote(id, note) {
        const noteElement = document.querySelector(`[data-note-id="${id}"] .card-body`);
        const originalContent = noteElement.innerHTML;
        noteElement.dataset.originalContent = originalContent;

        const form = `
            <form id="editForm-${id}" action="book.php?book_id=<?php echo $book_id; ?>" method="post">
                <input type="hidden" name="edit_note_id" value="${id}">
                <textarea name="edit_note" class="form-control" rows="2">${note}</textarea>
                <button type="submit" class="btn btn-primary mt-2" onclick="updateNote(${id}); return false;">Update</button>
                <button type="button" class="btn btn-secondary mt-2" onclick="cancelEdit(${id})">Cancel</button>
            </form>
        `;
        noteElement.innerHTML = form;

        return false;
    }

    function cancelEdit(id) {
        const noteElement = document.querySelector(`[data-note-id="${id}"] .card-body`);
        noteElement.innerHTML = noteElement.dataset.originalContent;
    }

    function updateNote(id) {
        document.getElementById(`editForm-${id}`).submit();
    }

  //komentarii  
 function showEditCommentForm(commentId) {
    const commentElement = document.querySelector(`[data-comment-id="${commentId}"]`);
    const commentContent = commentElement.querySelector('.comment-content');
    const editForm = commentElement.querySelector(`#editCommentForm-${commentId}`);
    const editButton = commentElement.querySelector(`[onclick="showEditCommentForm(${commentId})"]`);
    const deleteButton = commentElement.querySelector(`[name="delete_comment_id"][value="${commentId}"]`);

    commentContent.dataset.originalContent = commentContent.innerHTML;

    commentContent.style.display = 'none';
    editButton.style.display = 'none';
    deleteButton.style.display = 'none';
    editForm.style.display = 'block';
}

function cancelCommentEdit(commentId) {
    const commentElement = document.querySelector(`[data-comment-id="${commentId}"]`);
    const commentContent = commentElement.querySelector('.comment-content');
    const editForm = commentElement.querySelector(`#editCommentForm-${commentId}`);
    const editButton = commentElement.querySelector(`[onclick="showEditCommentForm(${commentId})"]`);
    const deleteButton = commentElement.querySelector(`[name="delete_comment_id"][value="${commentId}"]`);

    commentContent.innerHTML = commentContent.dataset.originalContent;

    editForm.style.display = 'none';
    commentContent.style.display = 'block';

    editButton.style.display = 'inline-block';
    deleteButton.style.display = 'inline-block';
}
    </script>
</div>
<?php include_once 'footer.php'; ?>
</body>
</html>
