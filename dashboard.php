<?php
session_start();
include_once 'config.php';
include_once 'header.php';

$user_id = isset($_SESSION['user_id']) ? $_SESSION['user_id'] : null;
$role = isset($_SESSION['role']) ? $_SESSION['role'] : null;
$isLoggedIn = $user_id !== null;

if ($isLoggedIn) {if (isset($_SESSION['role'])) {
    if ($_SESSION['role'] === 'admin') {
        header('Location: admin_dashboard.php');
        exit;
    }
    // Redirect to regular user dashboard or homepage if needed
} else {
    // Handle case where role is not set in session (e.g., redirect to login)
    header('Location: login.php');
    exit;
}
}

function getBooksQuery($conn, $selectedCategories = []) {
    $queryConditions = "WHERE books.deleted_at IS NULL";

    if (!empty($selectedCategories)) {
        $categoryIds = implode(',', $selectedCategories);
        $queryConditions .= " AND books.category_id IN ($categoryIds)";
    }
    $query = "
        SELECT
            books.*,
            authors.first_name AS author_first_name,
            authors.last_name AS author_last_name,
            categories.title AS category_title
        FROM
            books
        JOIN
            authors ON books.author_id = authors.id
        JOIN
            categories ON books.category_id = categories.id
        $queryConditions
    ";

    $booksQuery = $conn->query($query);
    return $booksQuery->fetch_all(MYSQLI_ASSOC);
}
$categoriesQuery = $conn->query("SELECT * FROM categories WHERE deleted_at IS NULL");
$categories = $categoriesQuery->fetch_all(MYSQLI_ASSOC);

$selectedCategories = [];

if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST['categories'])) {
    $selectedCategories = $_POST['categories'];
}
if (!empty($selectedCategories)) {
    $categoryIds = implode(',', $selectedCategories);

    $query = "
        SELECT
            books.*, 
            authors.first_name AS author_first_name, 
            authors.last_name AS author_last_name, 
            categories.title AS category_title
        FROM
            books
        JOIN
            authors ON books.author_id = authors.id
        JOIN
            categories ON books.category_id = categories.id
        WHERE
            books.category_id IN ($categoryIds)
            AND books.deleted_at IS NULL
            AND categories.deleted_at IS NULL
    ";

    $booksQuery = $conn->query($query);
    $books = $booksQuery->fetch_all(MYSQLI_ASSOC);
} else {
    $query = "
        SELECT
            books.*, 
            authors.first_name AS author_first_name, 
            authors.last_name AS author_last_name, 
            categories.title AS category_title
        FROM
            books
        JOIN
            authors ON books.author_id = authors.id
        JOIN
            categories ON books.category_id = categories.id
        WHERE
            books.deleted_at IS NULL
            AND categories.deleted_at IS NULL
    ";

    $booksQuery = $conn->query($query);
    $books = $booksQuery->fetch_all(MYSQLI_ASSOC);
}
$greeting = "Hello Guest!";
if ($isLoggedIn) {
    $sql = "SELECT username FROM users WHERE id = ?";
    $stmt = $conn->prepare($sql);
    $stmt->bind_param("i", $user_id);
    $stmt->execute();
    $stmt->bind_result($username);
    $stmt->fetch();
    $stmt->close();

    if (!empty($username)) {
        $greeting = "Hello " . htmlspecialchars($username) . "!";
    }
}
?>

<!doctype html>
<html lang="en">

<head>
    <title>Book Store</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="styles.css">
</head>

<body>

    <div class="banner d-flex justify-content-center align-items-center h1 text-warning">
        <?php echo $greeting; ?>
    </div>
    <main class="container my-4">
        <!-- Categories -->
        <form id="categoryForm" method="post">
    <div class="d-flex align-items-center mb-3 p-3">
        <div class="categories col flex-nowrap me-2">
            <?php foreach ($categories as $category) : ?>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="checkbox" name="categories[]" value="<?php echo $category['id']; ?>" id="category<?php echo $category['id']; ?>" <?php echo in_array($category['id'], $selectedCategories) ? 'checked' : ''; ?>>
                    <label class="form-check-label" for="category<?php echo $category['id']; ?>"><?php echo htmlspecialchars($category['title']); ?></label>
                </div>
            <?php endforeach; ?>
        </div>
        <button type="submit" class="btn btn-primary">Search</button>
        <a href="#" class="btn btn-secondary m-2" id="resetBtn">Reset</a>
    </div>
</form>
        <hr>
        <!-- Books Container -->
        <div id="booksContaineruser" class="row row-cols-1 pt-4 p-1 row-cols-md-3 g-4 mt-4">
            <?php foreach ($books as $book) : ?>
                <div class="card-hover col-md-4 col-sm-6" onclick="redirectToBook(<?php echo $book['id']; ?>)">
                    <div class="card mb-3 book-card">
                        <div class="row g-0">
                            <div class="height col-md-6">
                                <img src="<?php echo htmlspecialchars($book["image_url"]); ?>" class="img-fluid card-img-top rounded-start" alt="Book Cover">
                            </div>
                            <div class="height col-md-6">
                                <div class="card-body">
                                    <h5 class="card-title"><?php echo htmlspecialchars($book['title']); ?></h5>
                                    <p class="card-text">Author: <?php echo htmlspecialchars($book['author_first_name'] . ' ' . $book['author_last_name']); ?></p>
                                    <p class="card-text">Category: <?php echo htmlspecialchars($book['category_title']); ?></p>
                                    <a href="javascript:void(0);" id="bookButton" class="btn btn-primary mt-2" onclick="redirectToBook(<?php echo $book['id']; ?>)">View Book Details</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </main>
    <?php include_once 'footer.php'; ?>
    <script src="https://kit.fontawesome.com/67513cd76d.js" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.8/dist/umd/popper.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.min.js"></script>
    <script>
      document.getElementById('categoryForm').addEventListener('submit', function(event) {
    event.preventDefault();

    // Check if at least one checkbox is selected
    var checkboxes = document.querySelectorAll('input[name="categories[]"]:checked');
    console.log('Checked checkboxes:', checkboxes); // Log checked checkboxes

    if (checkboxes.length === 0) {
        alert('Please select at least one category.');
        return;
    }

    var formData = new FormData(this);
    for (var pair of formData.entries()) {
        console.log(pair[0] + ': ' + pair[1]); // Log form data
    }

    fetch('fetch_books_user.php', {
        method: 'POST',
        body: formData
    })
    .then(response => response.text())
    .then(html => {
        document.getElementById('booksContaineruser').innerHTML = html;
    })
    .catch(error => {
        console.error('Error fetching books:', error);
    });
});

function fetchBooks(formData) {
    $.ajax({
        url: 'fetch_books.php',
        method: 'POST',
        data: formData,
        success: function(html) {
            $('#booksContaineruser').html(html);
        },
        error: function(xhr, status, error) {
            console.error('Error fetching books:', error);
        }
    });
}

document.addEventListener('DOMContentLoaded', function() {
        // Function to reload the page and scroll to categoryForm section
        function reloadAndScrollToCategoryForm() {
            // Reload the page
            location.reload();

            // Scroll to the categoryForm section after a short delay to ensure the page reloads first
            setTimeout(function() {
                var categoryFormElement = document.getElementById('categoryForm');
                if (categoryFormElement) {
                    categoryFormElement.scrollIntoView({ behavior: 'smooth' });
                }
            }, 500); // Adjust the delay (in milliseconds) as needed
        }

        // Attach click event listener to the reset button
        var resetBtn = document.getElementById('resetBtn');
        if (resetBtn) {
            resetBtn.addEventListener('click', function(event) {
                event.preventDefault();
                reloadAndScrollToCategoryForm();
            });
        }
    });

        function redirectToBook(bookId) {
            window.location.href = 'book.php?book_id=' + bookId;
        }
    </script>
</body>

</html>
