$(document).ready(function() {
    window.addNote = function() {
        let note = $('#note').val();
        if (note) {
            $.ajax({
                type: 'POST',
                url: 'book1.php?book_id=' + <?php echo $book_id; ?>,
                data: { note: note },
                dataType: 'json',
                success: function(response) {
                    if (response.success) {
                        alert('Note added successfully.');
                        location.reload();
                    } else {
                        alert('Failed to add note.');
                    }
                }
            });
        }
    };

    window.editNoteForm = function(noteId) {
        let noteCard = $('div[data-note-id="' + noteId + '"]');
        let noteContent = noteCard.find('.card-body').attr('data-original-content');
        noteCard.find('.card-body').html(`
            <textarea class="form-control" id="edit-note-${noteId}">${noteContent}</textarea>
            <button class="btn btn-sm btn-outline-primary" onclick="saveEditNoteForm(${noteId})">Save</button>
            <button class="btn btn-sm btn-outline-secondary" onclick="cancelEditNoteForm(${noteId})">Cancel</button>
        `);
    };

    window.saveEditNoteForm = function(noteId) {
        let newContent = $('#edit-note-' + noteId).val();
        $.ajax({
            type: 'POST',
            url: 'book1.php?book_id=' + <?php echo $book_id; ?>,
            data: { edit_note: newContent, edit_note_id: noteId },
            dataType: 'json',
            success: function(response) {
                if (response.success) {
                    alert('Note edited successfully.');
                    location.reload();
                } else {
                    alert('Failed to edit note.');
                }
            }
        });
    };

    window.cancelEditNoteForm = function(noteId) {
        let noteCard = $('div[data-note-id="' + noteId + '"]');
        let originalContent = noteCard.find('.card-body').attr('data-original-content');
        noteCard.find('.card-body').html(`
            <p class="card-text">${originalContent}</p>
            <button class="btn btn-sm btn-outline-primary" onclick="editNoteForm(${noteId})">Edit</button>
            <button class="btn btn-sm btn-outline-danger" onclick="deleteNote(${noteId})">Delete</button>
        `);
    };

    window.deleteNote = function(noteId) {
        if (confirm('Are you sure you want to delete this note?')) {
            $.ajax({
                type: 'POST',
                url: 'book1.php?book_id=' + <?php echo $book_id; ?>,
                data: { delete_note_id: noteId },
                dataType: 'json',
                success: function(response) {
                    if (response.success) {
                        alert('Note deleted successfully.');
                        location.reload();
                    } else {
                        alert('Failed to delete note.');
                    }
                }
            });
        }
    };

    window.addComment = function() {
        let comment = $('#comment').val();
        if (comment) {
            $.ajax({
                type: 'POST',
                url: 'book1.php?book_id=' + <?php echo $book_id; ?>,
                data: { comment: comment },
                dataType: 'json',
                success: function(response) {
                    if (response.success) {
                        alert('Comment added successfully.');
                        location.reload();
                    } else {
                        alert('Failed to add comment.');
                    }
                }
            });
        }
    };

    window.editCommentForm = function(commentId) {
        let commentCard = $('div[data-comment-id="' + commentId + '"]');
        let commentContent = commentCard.find('.card-body').attr('data-original-content');
        commentCard.find('.card-body').html(`
            <textarea class="form-control" id="edit-comment-${commentId}">${commentContent}</textarea>
            <button class="btn btn-sm btn-outline-primary" onclick="saveEditCommentForm(${commentId})">Save</button>
            <button class="btn btn-sm btn-outline-secondary" onclick="cancelEditCommentForm(${commentId})">Cancel</button>
        `);
    };

    window.saveEditCommentForm = function(commentId) {
        let newContent = $('#edit-comment-' + commentId).val();
        $.ajax({
            type: 'POST',
            url: 'book1.php?book_id=' + <?php echo $book_id; ?>,
            data: { edit_comment: newContent, edit_comment_id: commentId },
            dataType: 'json',
            success: function(response) {
                if (response.success) {
                    alert('Comment edited successfully.');
                    location.reload();
                } else {
                    alert('Failed to edit comment.');
                }
            }
        });
    };

    window.cancelEditCommentForm = function(commentId) {
        let commentCard = $('div[data-comment-id="' + commentId + '"]');
        let originalContent = commentCard.find('.card-body').attr('data-original-content');
        commentCard.find('.card-body').html(`
            <p class="card-text">${originalContent}</p>
            <button class="btn btn-sm btn-outline-primary" onclick="editCommentForm(${commentId})">Edit</button>
            <button class="btn btn-sm btn-outline-danger" onclick="deleteComment(${commentId})">Delete</button>
        `);
    };

    window.deleteComment = function(commentId) {
        if (confirm('Are you sure you want to delete this comment?')) {
            $.ajax({
                type: 'POST',
                url: 'book1.php?book_id=' + <?php echo $book_id; ?>,
                data: { delete_comment_id: commentId },
                dataType: 'json',
                success: function(response) {
                    if (response.success) {
                        alert('Comment deleted successfully.');
                        location.reload();
                    } else {
                        alert('Failed to delete comment.');
                    }
                }
            });
        }
    };
});
