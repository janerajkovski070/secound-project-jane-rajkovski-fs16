<?php
session_start();
include_once 'config.php';

$selectedCategories = isset($_POST['categories']) ? $_POST['categories'] : [];
$categoryIds = implode(',', $selectedCategories);

$query = "
    SELECT
        books.*,
        authors.first_name AS author_first_name,
        authors.last_name AS author_last_name,
        categories.title AS category_title
    FROM
        books
    JOIN
        authors ON books.author_id = authors.id
    JOIN
        categories ON books.category_id = categories.id
    WHERE
        books.deleted_at IS NULL";

if (!empty($selectedCategories)) {
    $query .= " AND books.category_id IN ($categoryIds)";
}

$booksQuery = $conn->query($query);
$books = $booksQuery->fetch_all(MYSQLI_ASSOC);

foreach ($books as $book) {
    echo '<div class="card-hover col-md-4 col-sm-6">';
    echo '<div class="card mb-3 book-card">';
    echo '<div class="row g-0">';
    echo '<div class="height col-md-6">';
    echo '<img src="' . htmlspecialchars($book["image_url"]) . '" class="img-fluid card-img-top rounded-start" alt="Book Cover">';
    echo '</div>';
    echo '<div class="height col-md-6">';
    echo '<div class="card-body">';
    echo '<h5 class="card-title">' . htmlspecialchars($book['title']) . '</h5>';
    echo '<p class="card-text">Author: ' . htmlspecialchars($book['author_first_name'] . ' ' . $book['author_last_name']) . '</p>';
    echo '<p class="card-text">Category: ' . htmlspecialchars($book['category_title']) . '</p>';
    echo '<a href="javascript:void(0);" id="bookButton" class="btn btn-primary mt-2" onclick="redirectToBook(' . $book['id'] . ')">View Book Details</a>';
    echo '<a href="javascript:void(0);" class="btn btn-secondary mt-2 p-2" onclick="editBook(' . $book['id'] . ')">Edit</a>';
    echo '<button class="btn btn-danger delete-book mt-2" data-book-id="' . $book['id'] . '">Delete</button>';
    echo '</div>';
    echo '</div>';
    echo '</div>';
    echo '</div>';
    echo '</div>';
}
?>
