<?php
session_start();
include 'config.php';

if (!isset($_SESSION['user_id'])) {
    header("Location: login.php");
    exit();
}

$user_id = $_SESSION['user_id'];

if ($_SERVER['REQUEST_METHOD'] == 'POST') {    
    $book_id = isset($_GET['book_id']) ? intval($_GET['book_id']) : 0;

    $note = isset($_POST['note']) ? htmlspecialchars(trim($_POST['note'])) : '';

    if ($book_id > 0 && !empty($note)) {
        // Insert note into the database
        insertNoteInDatabase($user_id, $book_id, $note);

        // Redirect back to the book page
        header("Location: book.php?book_id=$book_id");
        exit();
    } else {
        echo "Error: Invalid book ID or empty note.";
        exit();
    }
} else {
    echo "Error: Invalid request method.";
    exit();
}

?>
