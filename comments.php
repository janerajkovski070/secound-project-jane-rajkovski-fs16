<?php
session_start();
include 'config.php';

if (!isset($_SESSION['user_id'])) {
    header("Location: login.php");
    exit();
}

$user_id = $_SESSION['user_id'];

if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST['comment'])) {
    $comment = htmlspecialchars(trim($_POST['comment']));
    $book_id = isset($_GET['book_id']) ? intval($_GET['book_id']) : 0;

    if ($comment && $book_id > 0) {
        $stmt = $conn->prepare("INSERT INTO comments (user_id, book_id, comment, approved, created_at) VALUES (?, ?, ?, 'pending', NOW())");
        $stmt->bind_param("iis", $user_id, $book_id, $comment);
        $stmt->execute();
        $stmt->close();

        header("Location: book.php?book_id=$book_id");
        exit();
    } else {
        echo "Error: Comment or Book ID is missing or invalid.";
        exit();
    }
} else {
    echo "Error: Form submission method not allowed.";
    exit();
}
