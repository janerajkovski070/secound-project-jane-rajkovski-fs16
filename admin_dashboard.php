<?php
session_start();
include_once 'config.php';
include_once 'header.php';

if (!isset($_SESSION['user_id']) || $_SESSION['role'] !== 'admin') {
    header("Location: dashboard.php");
    exit;
}

$user_id = $_SESSION['user_id'];
$isLoggedIn = true;

$greeting = "Hello Admin!";
if ($isLoggedIn) {
    $sql = "SELECT username FROM users WHERE id = ?";
    $stmt = $conn->prepare($sql);
    $stmt->bind_param("i", $user_id);
    $stmt->execute();
    $stmt->bind_result($username);
    $stmt->fetch();
    $stmt->close();

    if (!empty($username)) {
        $greeting = "Hello Admin, " . htmlspecialchars($username) . "!";
    }
}

// Fetch only active categories (not soft-deleted)
$categoriesQuery = $conn->query("SELECT * FROM categories WHERE deleted_at IS NULL");
$categories = $categoriesQuery->fetch_all(MYSQLI_ASSOC);

$selectedCategories = [];

if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST['categories'])) {
    $selectedCategories = $_POST['categories'];
}

if (!empty($selectedCategories)) {
    $categoryIds = implode(',', $selectedCategories);

    $query = "
        SELECT
            books.*, 
            authors.first_name AS author_first_name, 
            authors.last_name AS author_last_name, 
            categories.title AS category_title
        FROM
            books
        JOIN
            authors ON books.author_id = authors.id
        JOIN
            categories ON books.category_id = categories.id
        WHERE
            books.category_id IN ($categoryIds)
            AND books.deleted_at IS NULL
            AND categories.deleted_at IS NULL
            AND authors.deleted_at IS NULL  -- Ensure author is not deleted
    ";

    $booksQuery = $conn->query($query);
    $books = $booksQuery->fetch_all(MYSQLI_ASSOC);
} else {
    // Fetch all books from active categories
    $query = "
        SELECT
            books.*, 
            authors.first_name AS author_first_name, 
            authors.last_name AS author_last_name, 
            categories.title AS category_title
        FROM
            books
        JOIN
            authors ON books.author_id = authors.id
        JOIN
            categories ON books.category_id = categories.id
        WHERE
            books.deleted_at IS NULL
            AND categories.deleted_at IS NULL
            AND authors.deleted_at IS NULL
    ";

    $booksQuery = $conn->query($query);
    $books = $booksQuery->fetch_all(MYSQLI_ASSOC);
}
?>

<!doctype html>
<html lang="en">
<head>
    <title>Book Store - Admin</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="styles.css">
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
</head>
<body>
    <div class="banner d-flex justify-content-center align-items-center h1 text-warning">
        <?php echo $greeting; ?>
    </div>
    <main class="container my-4">
    <h2 class="pb-4">Admin panel</h2>
    <br>
    <div class="panel">
        <ul class="row d-flex justify-content-between pb-5 text-center">
            <li class="col-auto">
                <a href="../admin/comments_approve.php" class="nav-link">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 640 512" width="24" height="24">
                        <path d="M208 352c114.9 0 208-78.8 208-176S322.9 0 208 0S0 78.8 0 176c0 38.6 14.7 74.3 39.6 103.4c-3.5 9.4-8.7 17.7-14.2 24.7c-4.8 6.2-9.7 11-13.3 14.3c-1.8 1.6-3.3 2.9-4.3 3.7c-.5 .4-.9 .7-1.1 .8l-.2 .2 0 0 0 0C1 327.2-1.4 334.4 .8 340.9S9.1 352 16 352c21.8 0 43.8-5.6 62.1-12.5c9.2-3.5 17.8-7.4 25.3-11.4C134.1 343.3 169.8 352 208 352zM448 176c0 112.3-99.1 196.9-216.5 207C255.8 457.4 336.4 512 432 512c38.2 0 73.9-8.7 104.7-23.9c7.5 4 16 7.9 25.2 11.4c18.3 6.9 40.3 12.5 62.1 12.5c6.9 0 13.1-4.5 15.2-11.1c2.1-6.6-.2-13.8-5.8-17.9l0 0 0 0-.2-.2c-.2-.2-.6-.4-1.1-.8c-1-.8-2.5-2-4.3-3.7c-3.6-3.3-8.5-8.1-13.3-14.3c-5.5-7-10.7-15.4-14.2-24.7c24.9-29 39.6-64.7 39.6-103.4c0-92.8-84.9-168.9-192.6-175.5c.4 5.1 .6 10.3 .6 15.5z"/>
                    </svg>
                    <p>Coments Approval</p>
                    <span class="badge bg-danger" id="comments-badge">New</span>
                </a>
            </li>
            <li class="col-auto text-center">
                <a href="admin/add_book.php" class="nav-link">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" width="24" height="24">
                        <path d="M0 96C0 43 43 0 96 0H384h32c17.7 0 32 14.3 32 32V352c0 17.7-14.3 32-32 32v64c17.7 0 32 14.3 32 32s-14.3 32-32 32H384 96c-53 0-96-43-96-96V96zM64 416c0 17.7 14.3 32 32 32H352V384H96c-17.7 0-32 14.3-32 32zM208 112v48H160c-8.8 0-16 7.2-16 16v32c0 8.8 7.2 16 16 16h48v48c0 8.8 7.2 16 16 16h32c8.8 0 16-7.2 16-16V224h48c8.8 0 16-7.2 16-16V176c0-8.8-7.2-16-16-16H272V112c0-8.8-7.2-16-16-16H224c-8.8 0-16 7.2-16 16z"/>
                    </svg>
                    <p>Manage books</p>
                </a>
            </li>
            <li class="col-auto text-center">
                <a href="admin/add_author.php" class="nav-link">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512" width="24" height="24">
                        <path d="M112 48a48 48 0 1 1 96 0 48 48 0 1 1 -96 0zm40 304V480c0 17.7-14.3 32-32 32s-32-14.3-32-32V256.9L59.4 304.5c-9.1 15.1-28.8 20-43.9 10.9s-20-28.8-10.9-43.9l58.3-97c17.4-28.9 48.6-46.6 82.3-46.6h29.7c33.7 0 64.9 17.7 82.3 46.6l58.3 97c9.1 15.1 4.2 34.8-10.9 43.9s-34.8 4.2-43.9-10.9L232 256.9V480c0 17.7-14.3 32-32 32s-32-14.3-32-32V352H152z"/>
                    </svg>
                    <p>Manage authors</p>
                </a>
            </li>
            <li class="col-auto text-center">
                <a href="admin/add_category.php" class="nav-link">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" width="24" height="24">
                        <path d="M40 48C26.7 48 16 58.7 16 72v48c0 13.3 10.7 24 24 24H88c13.3 0 24-10.7 24-24V72c0-13.3-10.7-24-24-24H40zM192 64c-17.7 0-32 14.3-32 32s14.3 32 32 32H480c17.7 0 32-14.3 32-32s-14.3-32-32-32H192zm0 160c-17.7 0-32 14.3-32 32s14.3 32 32 32H480c17.7 0 32-14.3 32-32s-14.3-32-32-32H192zm0 160c-17.7 0-32 14.3-32 32s14.3 32 32 32H480c17.7 0 32-14.3 32-32s-14.3-32-32-32H192zM16 232v48c0 13.3 10.7 24 24 24H88c13.3 0 24-10.7 24-24V232c0-13.3-10.7-24-24-24H40c-13.3 0-24 10.7-24 24zM40 368c-13.3 0-24 10.7-24 24v48c0 13.3 10.7 24 24 24H88c13.3 0 24-10.7 24-24V392c0-13.3-10.7-24-24-24H40z"/>
                    </svg>
                    <p>Manage categoryes</p>
                </a>
            </li>
        </ul>
    </div>
        <!-- Categories Form -->
        <form id="categoryForm" method="post">
    <div class="d-flex align-items-center mb-3 p-3">
        <div class="categories col flex-nowrap me-2">
            <?php foreach ($categories as $category) : ?>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="checkbox" name="categories[]" value="<?php echo $category['id']; ?>" id="category<?php echo $category['id']; ?>" <?php echo in_array($category['id'], $selectedCategories) ? 'checked' : ''; ?>>
                    <label class="form-check-label" for="category<?php echo $category['id']; ?>"><?php echo htmlspecialchars($category['title']); ?></label>
                </div>
            <?php endforeach; ?>
        </div>
         <button type="submit" class="btn btn-primary">Search</button>
         <a href="#" class="btn btn-secondary m-2" id="resetBtn">Reset</a>
         </div>
</form>
        <hr>
        <!-- Books Container -->
        <div id="booksContainer" class="row row-cols-1 pt-4 p-1 row-cols-md-3 g-4 mt-4">
            <?php foreach ($books as $book) : ?>
                <div class="card-hover col-md-4 col-sm-6">
                    <div class="card mb-3 book-card">
                        <div class="row g-0">
                            <div class="height col-md-6">
                                <img src="<?php echo htmlspecialchars($book["image_url"]); ?>" class="img-fluid card-img-top rounded-start" alt="Book Cover">
                            </div>
                            <div class="height col-md-6">
                                <div class="card-body">
                                    <h5 class="card-title"><?php echo htmlspecialchars($book['title']); ?></h5>
                                    <p class="card-text">Author: <?php echo htmlspecialchars($book['author_first_name'] . ' ' . $book['author_last_name']); ?></p>
                                    <p class="card-text">Category: <?php echo htmlspecialchars($book['category_title']); ?></p>
                                    <a href="javascript:void(0);" id="bookButton" class="btn btn-primary mt-2" onclick="redirectToBook(<?php echo $book['id']; ?>)">View Book Details</a>
                                    <a href="javascript:void(0);" class="btn btn-secondary mt-2" onclick="editBook(<?php echo $book['id']; ?>)">Edit</a>
                                    <button class="btn btn-danger delete-book mt-2" data-book-id="<?php echo $book['id']; ?>">Delete</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </main>

    <?php include_once 'footer.php'; ?>

    <script src="https://kit.fontawesome.com/67513cd76d.js" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.8/dist/umd/popper.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.min.js"></script>
    <script>
         $(document).ready(function() {
            $('#categoryForm').submit(function(event) {
                event.preventDefault();
                var formData = new FormData(this);

                // Check if any checkboxes are checked
                var checkedCategories = $('input[name="categories[]"]:checked').length > 0;

                if (checkedCategories) {
                    fetchBooks(formData);
                } else {
                    // Show message or handle logic when no categories are selected
                    alert("Please select at least one category");
                }
            });
            function fetchBooks(formData) {
                fetch('fetch_books.php', {
                    method: 'POST',
                    body: formData
                })
                .then(response => response.text())
                .then(html => {
                    $('#booksContainer').html(html);
                })
                .catch(error => console.error('Error fetching books:', error));
            }
        });

        document.addEventListener('DOMContentLoaded', function() {
        // Function to reload the page and scroll to categoryForm section
        function reloadAndScrollToCategoryForm() {
            // Reload the page
            location.reload();

            // Scroll to the categoryForm section after a short delay to ensure the page reloads first
            setTimeout(function() {
                var categoryFormElement = document.getElementById('categoryForm');
                if (categoryFormElement) {
                    categoryFormElement.scrollIntoView({ behavior: 'smooth' });
                }
            }, 500); // Adjust the delay (in milliseconds) as needed
        }

        // Attach click event listener to the reset button
        var resetBtn = document.getElementById('resetBtn');
        if (resetBtn) {
            resetBtn.addEventListener('click', function(event) {
                event.preventDefault();
                reloadAndScrollToCategoryForm();
            });
        }
    });

        function redirectToBook(bookId) {
            window.location.href = 'book.php?book_id=' + bookId;
        }

        function editBook(bookId) {
            window.location.href = 'admin/edit_book.php?book_id=' + bookId;
        }

        function deleteBook(book_id) {
            if (confirm('Are you sure you want to delete this book?')) {
                $.ajax({
                    url: './admin/delete_book.php',
                    type: 'POST',
                    dataType: 'json',
                    data: {
                        book_id: book_id
                    },
                    success: function(response) {
                        if (response.success) {
                            alert('Book deleted successfully.');
                            location.reload();
                        } else {
                            alert('Failed to delete book.');
                        }
                    },
                    error: function(xhr, status, error) {
                        console.error('Error deleting book:', error);
                    }
                });
            }
        }

        $(document).ready(function() {
            $('.delete-book').on('click', function(e) {
                e.preventDefault();
                var book_id = $(this).data('book-id');
                Swal.fire({
                    title: 'Are you sure?',
                    text: "This action will delete the book and all associated comments. This action cannot be undone!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#d33',
                    cancelButtonColor: '#3085d6',
                    confirmButtonText: 'Yes, delete it!'
                }).then((result) => {
                    if (result.isConfirmed) {
                        $.ajax({
                            type: 'POST',
                            url: 'admin/delete_book.php',
                            data: { book_id: book_id },
                            dataType: 'json',
                            success: function(response) {
                                if (response.success) {
                                    Swal.fire(
                                        'Deleted!',
                                        'The book has been deleted.',
                                        'success'
                                    ).then(() => {
                                        window.location.reload();
                                    });
                                } else {
                                    Swal.fire(
                                        'Error!',
                                        'There was a problem deleting the book.',
                                        'error'
                                    );
                                }
                            },
                            error: function(xhr, status, error) {
                                Swal.fire(
                                    'Error!',
                                    'There was a problem deleting the book.',
                                    'error'
                                );
                            }
                        });
                    }
                });
            });
        });
    </script>
</body>
</html>