 <?php
session_start();
include_once '../config.php';

if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST['book_id'])) {
    $book_id = $_POST['book_id'];

    $sql = "UPDATE books SET deleted_at = NOW() WHERE id = ?";
    $stmt = $conn->prepare($sql);
    $stmt->bind_param("i", $book_id);

    if ($stmt->execute()) {
        $response = ['success' => true];
    } else {
        $response = ['success' => false];
    }

    $stmt->close();
} else {
    $response = ['success' => false];
}

header('Content-Type: application/json');
echo json_encode($response);
?>
