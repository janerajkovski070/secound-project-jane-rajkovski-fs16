<?php
include_once '../config.php';

if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST['id'])) {
    $comment_id = $_POST['id'];

    $sql_approve_comment = "UPDATE comments SET approved = 1 WHERE id = ?";
    $stmt_approve_comment = $conn->prepare($sql_approve_comment);
    $stmt_approve_comment->bind_param("i", $comment_id);

    $response = array();

    if ($stmt_approve_comment->execute()) {
        $response['success'] = true;
    } else {
        $response['success'] = false;
        $response['message'] = "Failed to approve comment. Please try again.";
    }

    $stmt_approve_comment->close();

    header('Content-Type: application/json');
    echo json_encode($response);
    exit;
}
?>
