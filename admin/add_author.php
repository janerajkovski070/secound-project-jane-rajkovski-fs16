<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Add Author</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="../styles.css">
</head>
<body>
    <?php include_once "../header.php"; ?>

    <div class="container mt-5">
        <h2>Add New Author</h2>
        <?php
        include_once '../config.php';

        $first_name = '';
        $last_name = '';
        $biography = '';
        $error = '';
        $success = '';

        if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST['submit'])) {
            $first_name = $_POST['first_name'];
            $last_name = $_POST['last_name'];
            $biography = $_POST['biography'];

            if (empty($first_name) || empty($last_name) || empty($biography)) {
                $error = "All fields are required.";
            } elseif (strlen($biography) < 20) {
                $error = "Biography must be at least 20 characters.";
            } else {
                $sql_check_author = "SELECT id FROM authors WHERE first_name = ? AND last_name = ?";
                $stmt_check_author = $conn->prepare($sql_check_author);
                $stmt_check_author->bind_param("ss", $first_name, $last_name);
                $stmt_check_author->execute();
                $stmt_check_author->store_result();

                if ($stmt_check_author->num_rows > 0) {
                    $error = "Author with the same name already exists.";
                } else {
                    $sql_insert_author = "INSERT INTO authors (first_name, last_name, biography) VALUES (?, ?, ?)";
                    $stmt_insert_author = $conn->prepare($sql_insert_author);
                    $stmt_insert_author->bind_param("sss", $first_name, $last_name, $biography);

                    if ($stmt_insert_author->execute()) {
                        $success = "Author added successfully.";
                        $first_name = $last_name = $biography = '';
                    } else {
                        $error = "Failed to add author. Please try again.";
                    }

                    $stmt_insert_author->close();
                }

                $stmt_check_author->close();
            }
        }

        if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST['delete']) && isset($_POST['id'])) {
            $author_id = $_POST['id'];

            // Soft delete 
            $sql_soft_delete = "UPDATE authors SET deleted_at = NOW() WHERE id = ?";
            $stmt_soft_delete = $conn->prepare($sql_soft_delete);
            $stmt_soft_delete->bind_param("i", $author_id);

            if ($stmt_soft_delete->execute()) {
                $success = "Author soft-deleted successfully.";
            } else {
                $error = "Failed to soft-delete author. Please try again.";
            }

            $stmt_soft_delete->close();

            header("Location: add_author.php");
            exit;
        }

        if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST['retrieve']) && isset($_POST['id'])) {
            $author_id = $_POST['id'];

            $sql_retrieve_author = "UPDATE authors SET deleted_at = NULL WHERE id = ?";
            $stmt_retrieve_author = $conn->prepare($sql_retrieve_author);
            $stmt_retrieve_author->bind_param("i", $author_id);

            if ($stmt_retrieve_author->execute()) {
                $success = "Author retrieved successfully.";
            } else {
                $error = "Failed to retrieve author. Please try again.";
            }
            $stmt_retrieve_author->close();
            header("Location: add_author.php");
            exit;
        }
        ?>

        <?php if (!empty($error)): ?>
            <div class="alert alert-danger"><?php echo $error; ?></div>
        <?php endif; ?>
        <?php if (!empty($success)): ?>
            <div class="alert alert-success"><?php echo $success; ?></div>
        <?php endif; ?>

        <form method="post">
            <div class="mb-3">
                <label for="first_name" class="form-label">First Name</label>
                <input type="text" class="form-control" id="first_name" name="first_name" value="<?php echo htmlspecialchars($first_name); ?>" required>
            </div>
            <div class="mb-3">
                <label for="last_name" class="form-label">Last Name</label>
                <input type="text" class="form-control" id="last_name" name="last_name" value="<?php echo htmlspecialchars($last_name); ?>" required>
            </div>
            <div class="mb-3">
                <label for="biography" class="form-label">Biography</label>
                <textarea class="form-control" id="biography" name="biography" rows="3" required><?php echo htmlspecialchars($biography); ?></textarea>
            </div>
            <button type="submit" name="submit" class="btn btn-primary">Add Author</button>
        </form>
    </div>

    <!-- List Active Authors -->
    <div class="container mt-5">
        <h2>Active Authors</h2>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Biography</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $sql_select_active_authors = "SELECT * FROM authors WHERE deleted_at IS NULL";
                $result_active = $conn->query($sql_select_active_authors);

                if ($result_active->num_rows > 0) {
                    while ($row = $result_active->fetch_assoc()) {
                        echo "<tr>";
                        echo "<td>" . htmlspecialchars($row['first_name']) . "</td>";
                        echo "<td>" . htmlspecialchars($row['last_name']) . "</td>";
                        echo "<td>" . htmlspecialchars($row['biography']) . "</td>";
                        echo "<td>";
                        echo "<form action='add_author.php' method='post' onsubmit='return confirm(\"Are you sure you want to soft delete this author?\")'>";
                        echo "<input type='hidden' name='id' value='{$row['id']}'>";
                        echo "<button type='submit' class='btn btn-sm btn-danger' name='delete'>Soft Delete</button>";
                        echo "</form>";
                        echo "</td>";
                        echo "</tr>";
                    }
                } else {
                    echo "<tr><td colspan='4'>No active authors found</td></tr>";
                }
                ?>
            </tbody>
        </table>
    </div>

    <!-- List Deleted Authors -->
    <div class="container mt-5 pb-4">
        <h2>Deleted Authors</h2>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Biography</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <?php
                // Fetch deleted authors (where deleted_at is NOT NULL)
                $sql_select_deleted_authors = "SELECT * FROM authors WHERE deleted_at IS NOT NULL";
                $result_deleted = $conn->query($sql_select_deleted_authors);

                if ($result_deleted->num_rows > 0) {
                    while ($row = $result_deleted->fetch_assoc()) {
                        echo "<tr>";
                        echo "<td>" . htmlspecialchars($row['first_name']) . "</td>";
                        echo "<td>" . htmlspecialchars($row['last_name']) . "</td>";
                        echo "<td>" . htmlspecialchars($row['biography']) . "</td>";
                        echo "<td>";
                        echo "<form action='add_author.php' method='post' onsubmit='return confirm(\"Are you sure you want to retrieve this author?\")'>";
                        echo "<input type='hidden' name='id' value='{$row['id']}'>";
                        echo "<button type='submit' class='btn btn-sm btn-success' name='retrieve'>Retrieve</button>";
                        echo "</form>";
                        echo "</td>";
                        echo "</tr>";
                    }
                } else {
                    echo "<tr><td colspan='4'>No deleted authors found</td></tr>";
                }
                ?>
            </tbody>
        </table>
    </div>
    <?php include_once '../footer.php'; ?>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"></script>
</body>
</html>
