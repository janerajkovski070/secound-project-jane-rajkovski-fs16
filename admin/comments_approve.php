<?php
        // Include database configuration
        include_once '../config.php';

        $error = '';
        $success = '';

        // Handle comment approval, rejection, and retrieval actions
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            if (isset($_POST['approve'])) {
                $comment_id = $_POST['id'];

                // Approve comment query
                $sql_approve_comment = "UPDATE comments SET approved = 1 WHERE id = ?";
                $stmt_approve_comment = $conn->prepare($sql_approve_comment);
                $stmt_approve_comment->bind_param("i", $comment_id);

                if ($stmt_approve_comment->execute()) {
                    $success = "Comment approved successfully.";
                } else {
                    $error = "Failed to approve comment. Please try again.";
                }

                $stmt_approve_comment->close();
            } elseif (isset($_POST['reject'])) {
                $comment_id = $_POST['id'];

                // Reject comment query (soft delete)
                $sql_reject_comment = "UPDATE comments SET approved = 2, deleted_at = NOW() WHERE id = ?";
                $stmt_reject_comment = $conn->prepare($sql_reject_comment);
                $stmt_reject_comment->bind_param("i", $comment_id);

                if ($stmt_reject_comment->execute()) {
                    $success = "Comment rejected and moved to deleted.";
                } else {
                    $error = "Failed to reject comment. Please try again.";
                }

                $stmt_reject_comment->close();
            } elseif (isset($_POST['retrieve'])) {
                $comment_id = $_POST['id'];

                // Retrieve comment query (undelete)
                $sql_retrieve_comment = "UPDATE comments SET approved = 0, deleted_at = NULL WHERE id = ?";
                $stmt_retrieve_comment = $conn->prepare($sql_retrieve_comment);
                $stmt_retrieve_comment->bind_param("i", $comment_id);

                if ($stmt_retrieve_comment->execute()) {
                    $success = "Comment retrieved successfully.";
                } else {
                    $error = "Failed to retrieve comment. Please try again.";
                }

                $stmt_retrieve_comment->close();
            }
        }

        $sql_select_pending_comments = "SELECT comments.id AS id, comments.comment AS text, books.title AS book_title, users.username AS user_name 
                                        FROM comments 
                                        JOIN books ON comments.book_id = books.id 
                                        JOIN users ON comments.user_id = users.id
                                        WHERE comments.approved = 0";
        $result_pending_comments = $conn->query($sql_select_pending_comments);
        ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Comments Management</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="../styles.css">
</head>
<body>
    <?php include_once "../header.php"; ?>

    <div class="container mt-5 pb-4">
        <h2>Comments Management</h2>
        <?php if (!empty($error)): ?>
            <div class="alert alert-danger"><?php echo $error; ?></div>
        <?php endif; ?>
        <?php if (!empty($success)): ?>
            <div class="alert alert-success"><?php echo $success; ?></div>
        <?php endif; ?>

        <h3>Pending Comments</h3>
        <div class="list-group">
            <?php if ($result_pending_comments->num_rows > 0): ?>
                <?php while ($row = $result_pending_comments->fetch_assoc()): ?>
                    <div class="list-group-item bg-secondary m-2" id="comment-<?php echo $row['id']; ?>">
                        <p><strong>User:</strong> <b><?php echo $row['user_name']; ?></b></p>
                        <p><?php echo $row['text']; ?></p>
                        <p><strong>Book:</strong> <?php echo $row['book_title']; ?></p>
                        <form action="comments_approve.php" method="post">
                            <input type="hidden" name="id" value="<?php echo $row['id']; ?>">
                            <button type="submit" class="btn btn-success approve-btn" name="approve">Approve</button>
                            <button type="submit" class="btn btn-danger reject-btn" name="reject">Reject</button>
                        </form>
                    </div>
                <?php endwhile; ?>
            <?php else: ?>
                <p>No pending comments.</p>
            <?php endif; ?>
        </div>

        <?php
        $sql_select_deleted_comments = "SELECT comments.id AS id, comments.comment AS text, books.title AS book_title, users.username AS user_name 
                                        FROM comments 
                                        JOIN books ON comments.book_id = books.id 
                                        JOIN users ON comments.user_id = users.id
                                        WHERE comments.approved = 2";
        $result_deleted_comments = $conn->query($sql_select_deleted_comments);
        ?>

        <h3>Rejected Comments</h3>
        <div class="list-group">
            <?php if ($result_deleted_comments->num_rows > 0): ?>
                <?php while ($row = $result_deleted_comments->fetch_assoc()): ?>
                    <div class="list-group-item bg-secondary m-2" id="comment-<?php echo $row['id']; ?>">
                        <p><strong>User:</strong> <b><?php echo $row['user_name']; ?></b></p>
                        <p><?php echo $row['text']; ?></p>
                        <p><strong>Book:</strong> <?php echo $row['book_title']; ?></p>
                        <form action="comments_approve.php" method="post">
                            <input type="hidden" name="id" value="<?php echo $row['id']; ?>">
                            <button type="submit" class="btn btn-success retrieve-btn" name="retrieve">Retrieve</button>
                        </form>
                    </div>
                <?php endwhile; ?>
            <?php else: ?>
                <p>No rejected comments.</p>
            <?php endif; ?>
        </div>
    </div>
    <?php include_once '../footer.php'; ?>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"></script>
</body>
</html>

<?php $conn->close(); ?>
