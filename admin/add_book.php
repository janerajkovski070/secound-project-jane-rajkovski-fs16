<?php
include_once '../config.php'; // za admin/

function fetchAuthors($conn) {
    $sql = "SELECT id, first_name, last_name FROM authors WHERE deleted_at IS NULL"; 
    $result = $conn->query($sql);
    $authors = [];
    if ($result->num_rows > 0) {
        while ($row = $result->fetch_assoc()) {
            $authors[] = $row;
        }
    }
    return $authors;
}

function fetchCategories($conn) {
    $sql = "SELECT id, title FROM categories WHERE deleted_at IS NULL";
    $result = $conn->query($sql);
    $categories = [];
    if ($result->num_rows > 0) {
        while ($row = $result->fetch_assoc()) {
            $categories[] = $row;
        }
    }
    return $categories;
}

function fetchDeletedBooks($conn) {
    $sql = "SELECT books.id, books.title, authors.first_name, authors.last_name, categories.title as category 
            FROM books 
            JOIN authors ON books.author_id = authors.id 
            JOIN categories ON books.category_id = categories.id 
            WHERE books.deleted_at IS NOT NULL";
    $result = $conn->query($sql);
    $deleted_books = [];
    if ($result->num_rows > 0) {
        while ($row = $result->fetch_assoc()) {
            $deleted_books[] = $row;
        }
    }
    return $deleted_books;
}

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if (isset($_POST['submit'])) {
        $title = htmlspecialchars($_POST['title']);
        $author_id = intval($_POST['author_id']);
        $category_id = intval($_POST['category_id']);
        $publication_year = intval($_POST['publication_year']);
        $page_count = intval($_POST['page_count']);
        $image_url = htmlspecialchars($_POST['image_url']);

        $sql = "INSERT INTO books (title, author_id, category_id, publication_year, page_count, image_url) VALUES (?, ?, ?, ?, ?, ?)";
        $stmt = $conn->prepare($sql);
        $stmt->bind_param("siisss", $title, $author_id, $category_id, $publication_year, $page_count, $image_url);

        if ($stmt->execute()) {
            $success_message = "Book added successfully.";
        } else {
            $error_message = "Error adding book: " . $conn->error;
        }

        $stmt->close();
    } elseif (isset($_POST['retrieve'])) {
        $book_id = intval($_POST['book_id']);
        $sql = "UPDATE books SET deleted_at = NULL WHERE id = ?";
        $stmt = $conn->prepare($sql);
        $stmt->bind_param("i", $book_id);

        if ($stmt->execute()) {
            $success_message = "Book retrieved successfully.";
        } else {
            $error_message = "Error retrieving book: " . $conn->error;
        }

        $stmt->close();
    }
}

$authors = fetchAuthors($conn);
$categories = fetchCategories($conn);
$deleted_books = fetchDeletedBooks($conn);

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Add Book</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous" />
    <link rel="stylesheet" href="../styles.css">
</head>
<body>
    <?php include_once "../header.php"; ?>
    <div class="container mt-4 pb-4">
        <h2>Add New Book</h2>
        <?php if (isset($success_message)) : ?>
            <div class="alert alert-success" role="alert">
                <?php echo $success_message; ?>
            </div>
        <?php elseif (isset($error_message)) : ?>
            <div class="alert alert-danger" role="alert">
                <?php echo $error_message; ?>
            </div>
        <?php endif; ?>
        <form method="POST" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
            <div class="mb-3">
                <label for="title" class="form-label">Title</label>
                <input type="text" class="form-control" id="title" name="title" required>
            </div>
            <div class="mb-3">
                <label for="author_id" class="form-label">Author</label>
                <select class="form-select" id="author_id" name="author_id" required>
                    <option value="">Select an author</option>
                    <?php foreach ($authors as $author) : ?>
                        <option value="<?php echo $author['id']; ?>"><?php echo $author['first_name'] . ' ' . $author['last_name']; ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
            <div class="mb-3">
                <label for="category_id" class="form-label">Category</label>
                <select class="form-select" id="category_id" name="category_id" required>
                    <option value="">Select a category</option>
                    <?php foreach ($categories as $category) : ?>
                        <option value="<?php echo $category['id']; ?>"><?php echo $category['title']; ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
            <div class="mb-3">
                <label for="publication_year" class="form-label">Publication Year</label>
                <input type="number" class="form-control" id="publication_year" name="publication_year" required>
            </div>
            <div class="mb-3">
                <label for="page_count" class="form-label">Page Count</label>
                <input type="number" class="form-control" id="page_count" name="page_count" required>
            </div>
            <div class="mb-3">
                <label for="image_url" class="form-label">Image URL</label>
                <input type="text" class="form-control" id="image_url" name="image_url">
            </div>
            <button type="submit" class="btn btn-primary" name="submit">Add Book</button>
        </form>

        <h2 class="mt-4">Soft-Deleted Books</h2>
        <div class="list-group">
            <?php if (!empty($deleted_books)) : ?>
                <?php foreach ($deleted_books as $book) : ?>
                    <div class="list-group-item bg-secondary">
                        <h5 class="mb-1"><?php echo $book['title']; ?></h5>
                        <p class="mb-1"><strong>Author:</strong> <?php echo $book['first_name'] . ' ' . $book['last_name']; ?></p>
                        <p class="mb-1"><strong>Category:</strong> <?php echo $book['category']; ?></p>
                        <form method="POST" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
                            <input type="hidden" name="book_id" value="<?php echo $book['id']; ?>">
                            <button type="submit" class="btn btn-success" name="retrieve">Retrieve</button>
                        </form>
                    </div>
                <?php endforeach; ?>
            <?php else : ?>
                <p>No soft-deleted books found.</p>
            <?php endif; ?>
        </div>
    </div>
    <?php include_once '../footer.php'; ?>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.min.js"></script>
</body>
</html>

<?php
$conn->close();
?>
