<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Add Category</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="../styles.css">
</head>
<body>
    <?php include_once "../header.php"; ?>

    <div class="container mt-5">
        <h2>Add New Category</h2>
        <?php
        include_once '../config.php';

        $title = '';
        $error = '';
        $success = '';

        if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST['submit'])) {
            $title = $_POST['title'];

            if (empty($title)) {
                $error = "Category title is required.";
            } else {
                $sql_check_category = "SELECT id FROM categories WHERE title = ?";
                $stmt_check_category = $conn->prepare($sql_check_category);
                $stmt_check_category->bind_param("s", $title);
                $stmt_check_category->execute();
                $stmt_check_category->store_result();

                if ($stmt_check_category->num_rows > 0) {
                    $error = "Category with the same title already exists.";
                } else {
                    $sql_insert_category = "INSERT INTO categories (title) VALUES (?)";
                    $stmt_insert_category = $conn->prepare($sql_insert_category);
                    $stmt_insert_category->bind_param("s", $title);

                    if ($stmt_insert_category->execute()) {
                        $success = "Category added successfully.";
                        $title = '';
                    } else {
                        $error = "Failed to add category. Please try again.";
                    }

                    $stmt_insert_category->close();
                }

                $stmt_check_category->close();
            }
        }

        if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST['delete']) && isset($_POST['id'])) {
            $category_id = $_POST['id'];

            $sql_soft_delete = "UPDATE categories SET deleted_at = NOW() WHERE id = ?";
            $stmt_soft_delete = $conn->prepare($sql_soft_delete);
            $stmt_soft_delete->bind_param("i", $category_id);

            if ($stmt_soft_delete->execute()) {
                $success = "Category soft-deleted successfully.";
            } else {
                $error = "Failed to soft-delete category. Please try again.";
            }

            $stmt_soft_delete->close();

            header("Location: add_category.php");
            exit;
        }
        if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST['retrieve']) && isset($_POST['id'])) {
            $category_id = $_POST['id'];

            $sql_retrieve_category = "UPDATE categories SET deleted_at = NULL WHERE id = ?";
            $stmt_retrieve_category = $conn->prepare($sql_retrieve_category);
            $stmt_retrieve_category->bind_param("i", $category_id);

            if ($stmt_retrieve_category->execute()) {
                $success = "Category retrieved successfully.";
            } else {
                $error = "Failed to retrieve category. Please try again.";
            }
            $stmt_retrieve_category->close();
            header("Location: add_category.php");
            exit;
        }
        ?>

        <?php if (!empty($error)): ?>
            <div class="alert alert-danger"><?php echo $error; ?></div>
        <?php endif; ?>
        <?php if (!empty($success)): ?>
            <div class="alert alert-success"><?php echo $success; ?></div>
        <?php endif; ?>

        <form method="post">
            <div class="mb-3">
                <label for="title" class="form-label">Category Title</label>
                <input type="text" class="form-control" id="title" name="title" value="<?php echo htmlspecialchars($title); ?>" required>
            </div>
            <button type="submit" name="submit" class="btn btn-primary">Add Category</button>
        </form>
    </div>
    <!-- List  Categories -->
    <div class="container mt-5">
        <h2>Active Categories</h2>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>Title</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $sql_select_active_categories = "SELECT * FROM categories WHERE deleted_at IS NULL";
                $result_active = $conn->query($sql_select_active_categories);

                if ($result_active->num_rows > 0) {
                    while ($row = $result_active->fetch_assoc()) {
                        echo "<tr>";
                        echo "<td>" . htmlspecialchars($row['title']) . "</td>";
                        echo "<td>";
                        echo "<form action='add_category.php' method='post' onsubmit='return confirm(\"Are you sure you want to soft delete this category?\")'>";
                        echo "<input type='hidden' name='id' value='{$row['id']}'>";
                        echo "<button type='submit' class='btn btn-sm btn-danger' name='delete'>Soft Delete</button>";
                        echo "</form>";
                        echo "</td>";
                        echo "</tr>";
                    }
                } else {
                    echo "<tr><td colspan='2'>No active categories found</td></tr>";
                }
                ?>
            </tbody>
        </table>
    </div>
    <!-- List Deleted Categories -->
    <div class="container mt-5 pb-4">
        <h2>Deleted Categories</h2>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>Title</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $sql_select_deleted_categories = "SELECT * FROM categories WHERE deleted_at IS NOT NULL";
                $result_deleted = $conn->query($sql_select_deleted_categories);

                if ($result_deleted->num_rows > 0) {
                    while ($row = $result_deleted->fetch_assoc()) {
                        echo "<tr>";
                        echo "<td>" . htmlspecialchars($row['title']) . "</td>";
                        echo "<td>";
                        echo "<form action='add_category.php' method='post' onsubmit='return confirm(\"Are you sure you want to retrieve this category?\")'>";
                        echo "<input type='hidden' name='id' value='{$row['id']}'>";
                        echo "<button type='submit' class='btn btn-sm btn-success' name='retrieve'>Retrieve</button>";
                        echo "</form>";
                        echo "</td>";
                        echo "</tr>";
                    }
                } else {
                    echo "<tr><td colspan='2'>No deleted categories found</td></tr>";
                }
                ?>
            </tbody>
        </table>
    </div>
    <?php include_once '../footer.php'; ?>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"></script>
</body>
</html>
