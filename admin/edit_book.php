<?php
session_start();
include_once '../config.php';

if (!isset($_SESSION['user_id']) || !isset($_SESSION['role']) || $_SESSION['role'] !== 'admin') {
    header("Location: dashboard.php");
    exit;
}

$book_id = isset($_GET['book_id']) ? intval($_GET['book_id']) : 0;
$book = null;

if ($book_id > 0) {
    $stmt = $conn->prepare("
        SELECT 
            books.*, 
            authors.first_name AS author_first_name, 
            authors.last_name AS author_last_name, 
            categories.title AS category_title 
        FROM 
            books 
        JOIN 
            authors ON books.author_id = authors.id 
        JOIN 
            categories ON books.category_id = categories.id 
        WHERE 
            books.id = ?
    ");
    $stmt->bind_param("i", $book_id);
    $stmt->execute();
    $result = $stmt->get_result();
    $book = $result->fetch_assoc();
    $stmt->close();
}

$stmt_categories = $conn->query("SELECT id, title FROM categories ORDER BY title");
$categories = [];
while ($row = $stmt_categories->fetch_assoc()) {
    $categories[] = $row;
}
$stmt_categories->close();

$stmt_authors = $conn->query("SELECT id, first_name, last_name FROM authors ORDER BY last_name, first_name");
$authors = [];
while ($row = $stmt_authors->fetch_assoc()) {
    $authors[] = $row;
}
$stmt_authors->close();

$errors = [];

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $title = isset($_POST['title']) ? trim($_POST['title']) : '';
    $author_id = isset($_POST['author_id']) ? intval($_POST['author_id']) : 0;
    $category_id = isset($_POST['category_id']) ? intval($_POST['category_id']) : 0;
    $publication_year = isset($_POST['publication_year']) ? intval($_POST['publication_year']) : 0;
    $page_count = isset($_POST['page_count']) ? intval($_POST['page_count']) : 0;
    $image_url = isset($_POST['image_url']) ? trim($_POST['image_url']) : '';

    if ($book_id > 0 && $title && $author_id > 0 && $category_id > 0 && $publication_year > 0 && $page_count > 0 && $image_url) {
        $stmt = $conn->prepare("
            UPDATE books 
            SET 
                title = ?, 
                author_id = ?, 
                category_id = ?, 
                publication_year = ?, 
                page_count = ?, 
                image_url = ? 
            WHERE 
                id = ?
        ");
        $stmt->bind_param("siisisi", $title, $author_id, $category_id, $publication_year, $page_count, $image_url, $book_id);
        $stmt->execute();

        if ($stmt->affected_rows > 0) {
            header("Location: ../book.php?book_id=$book_id");
    exit();
        } else {
            $errors[] = "Failed to update book. Please try changing some values.";
        }
        $stmt->close();
    } else {
        $errors[] = "Invalid input data. Please fill all fields.";
    }
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Edit Book</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="../styles.css">
</head>
<body>

<?php include '../header.php'; ?>

<div class="container mt-5 pb-4">
    <h2>Edit Book</h2>
    
    <?php if (!empty($errors)): ?>
        <div class="alert alert-danger">
            <ul>
                <?php foreach ($errors as $error): ?>
                    <li><?php echo $error; ?></li>
                <?php endforeach; ?>
            </ul>
        </div>
    <?php endif; ?>

    <form action="edit_book.php?book_id=<?php echo $book_id; ?>" method="POST">
        <div class="mb-3">
            <label for="title" class="form-label">Title</label>
            <input type="text" class="form-control" id="title" name="title" value="<?php echo htmlspecialchars($book['title'] ?? ''); ?>">
        </div>
        <div class="mb-3">
            <label for="author_id" class="form-label">Author</label>
            <select class="form-select" id="author_id" name="author_id">
                <?php foreach ($authors as $author) : ?>
                    <option value="<?php echo $author['id']; ?>" <?php echo ($book['author_id'] == $author['id']) ? 'selected' : ''; ?>>
                        <?php echo htmlspecialchars($author['first_name'] . ' ' . $author['last_name']); ?>
                    </option>
                <?php endforeach; ?>
            </select>
        </div>
        <div class="mb-3">
            <label for="category_id" class="form-label">Category</label>
            <select class="form-select" id="category_id" name="category_id">
                <?php foreach ($categories as $category) : ?>
                    <option value="<?php echo $category['id']; ?>" <?php echo ($book['category_id'] == $category['id']) ? 'selected' : ''; ?>>
                        <?php echo htmlspecialchars($category['title']); ?>
                    </option>
                <?php endforeach; ?>
            </select>
        </div>
        <div class="mb-3">
            <label for="publication_year" class="form-label">Publication Year</label>
            <input type="number" class="form-control" id="publication_year" name="publication_year" value="<?php echo htmlspecialchars($book['publication_year'] ?? ''); ?>">
        </div>
        <div class="mb-3">
            <label for="page_count" class="form-label">Page Count</label>
            <input type="number" class="form-control" id="page_count" name="page_count" value="<?php echo htmlspecialchars($book['page_count'] ?? ''); ?>">
        </div>
        <div class="mb-3">
            <label for="image_url" class="form-label">Image URL</label>
            <input type="text" class="form-control" id="image_url" name="image_url" value="<?php echo htmlspecialchars($book['image_url'] ?? ''); ?>">
        </div>
        <button type="submit" class="btn btn-primary">Update Book</button>
    </form>
</div>
<?php include_once '../footer.php'; ?>
</body>
</html>
