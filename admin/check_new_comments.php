<?php
include_once 'config.php';
session_start();

$sql_count_pending_comments = "SELECT COUNT(*) AS new_comments FROM comments WHERE approved = 0";
$result = $conn->query($sql_count_pending_comments);

if ($result) {
    $row = $result->fetch_assoc();
    $newCommentsCount = $row['new_comments'];
    echo json_encode(['new_comments' => $newCommentsCount]);
} else {
    echo json_encode(['new_comments' => 0]); // Return 0 in case of error or no comments
}

$conn->close();
